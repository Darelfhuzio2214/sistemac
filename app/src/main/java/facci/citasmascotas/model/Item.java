package facci.citasmascotas.model;

public class Item {
    String name_Item, description_Item,  code_Item , amount_Item, price_Item;
    public Item(){}

    public Item(String name_item, String description_item, String code_item, String amount_item, String price_item){
        this.name_Item = name_item;
        this.description_Item = description_item;
        this.code_Item = code_item;
        this.amount_Item = amount_item;
        this.price_Item = price_item;
    }

    public String getName_Item() {
        return name_Item;
    }

    public void setName_Item(String name_Item) {
        this.name_Item = name_Item;
    }

    public String getDescription_Item() {
        return description_Item;
    }

    public void setDescription_Item(String description_Item) {
        this.description_Item = description_Item;
    }

    public String getCode_Item() {
        return code_Item;
    }

    public void setCode_Item(String code_Item) {
        this.code_Item = code_Item;
    }

    public String getAmount_Item() {
        return amount_Item;
    }

    public void setAmount_Item(String amount_Item) {
        this.amount_Item = amount_Item;
    }

    public String getPrice_Item() {
        return price_Item;
    }

    public void setPrice_Item(String price_Item) {
        this.price_Item = price_Item;
    }
}

