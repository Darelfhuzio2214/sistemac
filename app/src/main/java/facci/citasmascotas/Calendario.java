package facci.citasmascotas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class Calendario extends AppCompatActivity {
    private EditText nomb_dog;
    private ImageView regresar, salir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendario);
        nomb_dog = findViewById(R.id.nomb_dog);
        regresar = findViewById(R.id.regresar);
        salir = findViewById(R.id.salir);

        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Calendario.this, Detalle_mascota.class);
                finish();
                startActivity(intent);
            }
        });

        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Calendario.this, MainActivity.class);
                finish();
                startActivity(intent);
            }
        });
    }
}