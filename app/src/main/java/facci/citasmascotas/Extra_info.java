package facci.citasmascotas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Extra_info extends AppCompatActivity {
    private ImageView logout_icon, return_icon,edit_info;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extra_info);

        return_icon = findViewById(R.id.extra_info_return_icon);
        logout_icon = findViewById(R.id.extra_info_logout_icon);
        edit_info= findViewById(R.id.edit_info);
        return_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        logout_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Extra_info.this, MainActivity.class);
                finish();
                startActivity(intent);
            }
        });
        edit_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Extra_info.this, editar_perfil.class);
                finish();
                startActivity(intent);
            }
        });
    }
}