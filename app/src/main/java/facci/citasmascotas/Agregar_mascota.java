package facci.citasmascotas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class Agregar_mascota extends AppCompatActivity {
    private CircleImageView user_icon ;
    private ImageView logout_icon;
    private LinearLayout AddItemActivity;
    Button new_item;
    EditText NameItem, CodeItem, DescriptionItem, AmountItem, PriceItem;
    private FirebaseFirestore mfirestore;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_mascota);

        String id = getIntent().getStringExtra("id_item");
        mfirestore = FirebaseFirestore.getInstance();

        user_icon = findViewById(R.id.agregar_mascota_user_icon);
        logout_icon = findViewById(R.id.agregar_mascota_logout_icon);
        new_item= findViewById(R.id.new_item);

        //Tablas
        NameItem = findViewById(R.id.NameItem);
        CodeItem = findViewById(R.id.CodeItem);
        DescriptionItem = findViewById(R.id.DescriptionItem);
        AmountItem = findViewById(R.id.AmountItem);
        PriceItem = findViewById(R.id.PriceItem);

        if (id == null || id ==""){

            new_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String nameItem = NameItem.getText().toString().trim();
                    String codeItem = CodeItem.getText().toString().trim();
                    String descriptionItem = DescriptionItem.getText().toString().trim();
                    String amountItem = AmountItem.getText().toString().trim();
                    String priceItem = PriceItem.getText().toString().trim();

                    if(nameItem.isEmpty() && codeItem.isEmpty() && descriptionItem.isEmpty() && amountItem.isEmpty() && priceItem.isEmpty()){
                        Toast.makeText(getApplicationContext(), "Ingresar datos de producto", Toast.LENGTH_SHORT).show();
                    }else{
                        postItem(nameItem, codeItem, descriptionItem, amountItem, priceItem);
                    }
                }
            });

        }else{
            new_item.setText("Update");
            getItem(id);
            new_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String nameItem = NameItem.getText().toString().trim();
                    String codeItem = CodeItem.getText().toString().trim();
                    String descriptionItem = DescriptionItem.getText().toString().trim();
                    String amountItem = AmountItem.getText().toString().trim();
                    String priceItem = PriceItem.getText().toString().trim();

                    if(nameItem.isEmpty() && codeItem.isEmpty() && descriptionItem.isEmpty() && amountItem.isEmpty() && priceItem.isEmpty()){
                        Toast.makeText(getApplicationContext(), "Ingresar de producto", Toast.LENGTH_SHORT).show();
                    }else{
                        updatePet(nameItem, codeItem, descriptionItem, amountItem, priceItem, id);
                    }
                }
            });

        }


        //VOLVER AL INICIO
        user_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        logout_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Agregar_mascota.this, MainActivity.class);
                finish();
                startActivity(intent);
            }
        });
    }

    private void updatePet(String nameItem, String codeItem, String descriptionItem, String amountItem, String priceItem, String id) {
        Map<String, Object> map = new HashMap<>();
        map.put("name_Item", nameItem);
        map.put("code_Item", codeItem);
        map.put("description_Item", descriptionItem);
        map.put("amount_Item", amountItem);
        map.put("price_Item", priceItem);

        mfirestore.collection("Items").document(id).update(map).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Toast.makeText(getApplicationContext(), "Actualizado exitosamente", Toast.LENGTH_SHORT).show();
                finish();
                Log.e("AddItem", "Producto actualizada exitosamente");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "Error en la actualización", Toast.LENGTH_SHORT).show();
                Log.e("Add", " Error en la actualización del producto");
            }
        });
    }

    private void postItem(String nameItem, String codeItem, String descriptionItem, String amountItem, String priceItem) {
        Map<String, Object> map = new HashMap<>();
        map.put("name_Item", nameItem);
        map.put("code_Item", codeItem);
        map.put("description_Item", descriptionItem);
        map.put("amount_Item", amountItem);
        map.put("price_Item", priceItem);

        mfirestore.collection("Items").add(map).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                Toast.makeText(getApplicationContext(), "Registro exitoso", Toast.LENGTH_SHORT).show();
                Log.e("AddPet", "Mascota agregada exitosamente");

                Intent intent= new Intent(Agregar_mascota.this, Listado_mascotas.class);
                finish();
                startActivity(intent);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "Error al registro", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getItem(String id){
        mfirestore.collection("Items").document(id).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                String name_Item = documentSnapshot.getString("name_Item");
                String code_Item = documentSnapshot.getString("code_Item");
                String description_Item = documentSnapshot.getString("description_Item");
                String amount_Item = documentSnapshot.getString("amount_Item");
                String price_Item = documentSnapshot.getString("price_Item");

                NameItem.setText(name_Item);
                CodeItem.setText(code_Item);
                DescriptionItem.setText(description_Item);
                AmountItem.setText(amount_Item);
                PriceItem.setText(price_Item);


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "Error al obtener los datos", Toast.LENGTH_SHORT).show();
                Log.e("AddItem", "Error al obtener los datos de Item");
            }
        });
    }

}