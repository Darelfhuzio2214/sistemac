package facci.citasmascotas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import de.hdodenhof.circleimageview.CircleImageView;

public class editar_perfil extends AppCompatActivity {

    private CircleImageView return_icon;
    private ImageView logout_icon;
    private LinearLayout guardar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_perfil);

        return_icon = findViewById(R.id.editar_perfil_return_icon);
        logout_icon = findViewById(R.id.editar_perfil_logout_icon);
        guardar = findViewById(R.id.editar_perfil_guardar);

        return_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        logout_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(editar_perfil.this, MainActivity.class);
                finish();
                startActivity(intent);
            }
        });

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(editar_perfil.this, EstadoGuardado.class);
                finish();
                startActivity(intent);
            }
        });
    }
}