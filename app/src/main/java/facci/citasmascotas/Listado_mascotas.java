package facci.citasmascotas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import de.hdodenhof.circleimageview.CircleImageView;
import facci.citasmascotas.adapter.ItemAdapter;
import facci.citasmascotas.model.Item;

public class Listado_mascotas extends AppCompatActivity {
    RecyclerView mRecycler;
    ItemAdapter mAdapter;
    FirebaseFirestore mFirestore;

    private CircleImageView user_icon ;
    private ImageView logout_icon;
    private LinearLayout agregar_item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_mascotas);

        mFirestore = FirebaseFirestore.getInstance();
        mRecycler = findViewById(R.id.recyclerViewSingle);
        mRecycler.setLayoutManager(new LinearLayoutManager(this));
        Query query = mFirestore.collection("Items");

        FirestoreRecyclerOptions<Item> firestoreRecyclerOptions =
                new FirestoreRecyclerOptions.Builder<Item>().setQuery(query, Item.class).build();

        mAdapter = new ItemAdapter(firestoreRecyclerOptions, this);
        mAdapter.notifyDataSetChanged();
        mRecycler.setAdapter(mAdapter);


        user_icon = findViewById(R.id.listado_mascotas_user_icon);
        logout_icon = findViewById(R.id.listado_mascotas_logout_icon);

        agregar_item = findViewById(R.id.listado_mascotas_agregar);


        user_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Listado_mascotas.this, Agregar_mascota.class);
                startActivity(intent);
            }
        });

        logout_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Listado_mascotas.this, MainActivity.class);
                finish();
                startActivity(intent);
            }
        });

        agregar_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Listado_mascotas.this, Agregar_mascota.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAdapter.startListening();

    }

    @Override
    protected void onStop() {
        super.onStop();
        mAdapter.stopListening();
    }
}