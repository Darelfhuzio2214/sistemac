package facci.citasmascotas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import de.hdodenhof.circleimageview.CircleImageView;

public class Detalle_mascota extends AppCompatActivity {
    private ImageView logout_icon, return_icon,angendar_cita,calendario,medico,receta_medica,carnet_vacuna;
    private LinearLayout info_icon, vacunas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_mascota);

        angendar_cita = findViewById(R.id.angendar_cita);
        calendario = findViewById(R.id.calendario);
        return_icon = findViewById(R.id.detalle_mascota_return_icon);
        logout_icon = findViewById(R.id.detalle_mascota_logout_icon);
        info_icon = findViewById(R.id.detalle_mascota_info);
        vacunas = findViewById(R.id.detalle_mascota_vacunas);
        medico = findViewById(R.id.medico);
        receta_medica = findViewById(R.id.receta_medica);
        carnet_vacuna = findViewById(R.id.caenet_vacuna);


        return_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Detalle_mascota.this, Listado_mascotas.class);
                startActivity(intent);
            }
        });

        logout_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Detalle_mascota.this, MainActivity.class);
                finish();
                startActivity(intent);
            }
        });

        info_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Detalle_mascota.this, Extra_info.class);
                startActivity(intent);
            }
        });

        vacunas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Detalle_mascota.this, Ingresar_vacuna.class);
                startActivity(intent);
            }
        });

        angendar_cita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Detalle_mascota.this, Agendar_cita.class);
                startActivity(intent);
            }
        });
        calendario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Detalle_mascota.this, Calendario.class);
                startActivity(intent);
            }
        });
        medico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Detalle_mascota.this, Medico.class);
                startActivity(intent);
            }
        });
        receta_medica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Detalle_mascota.this, Datos_medico.class);
                startActivity(intent);
            }
        });
        carnet_vacuna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Detalle_mascota.this, Carnet_Digital.class);
                startActivity(intent);
            }
        });
    }
}