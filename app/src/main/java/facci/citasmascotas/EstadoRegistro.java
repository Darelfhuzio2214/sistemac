package facci.citasmascotas;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class EstadoRegistro extends AppCompatActivity {

    private TextView init;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estado_registro);


        init=findViewById(R.id.init);



        init.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EstadoRegistro.this, LoginActivity.class);
                finish();
                startActivity(intent);
            }
        });
    }
}
