package facci.citasmascotas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

public class Carnet_Digital extends AppCompatActivity {

    private EditText nom_carnet;
    private ImageView regresar, salir;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carnet_digital);
        nom_carnet= findViewById(R.id.nom_carnet);
        regresar = findViewById(R.id.regresar);
        salir = findViewById(R.id.salir);

        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Carnet_Digital.this, Detalle_mascota.class);
                finish();
                startActivity(intent);
            }
        });

        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Carnet_Digital.this, MainActivity.class);
                finish();
                startActivity(intent);
            }
        });
    }

}