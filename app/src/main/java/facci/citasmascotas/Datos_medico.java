package facci.citasmascotas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class Datos_medico extends AppCompatActivity {

    private Button guardar;
    private ImageView regresar, salir;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_medico);

        guardar = findViewById(R.id.guardar);
        regresar = findViewById(R.id.regresar);
        salir = findViewById(R.id.salir);


        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Datos_medico.this, Detalle_mascota.class);
                finish();
                startActivity(intent);
            }
        });

        guardar.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent= new Intent(Datos_medico.this, EstadoGuardado.class);
               finish();
               startActivity(intent);
           }
       });

        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Datos_medico.this, MainActivity.class);
                finish();
                startActivity(intent);
            }
        });

    }

}