package facci.citasmascotas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class Ingresar_vacuna extends AppCompatActivity {
    private ImageView logout_icon, return_icon;
    private LinearLayout info_icon;
    private Button guardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingresar_vacuna);

        return_icon = findViewById(R.id.cerrarJirafa);
        logout_icon = findViewById(R.id.layaut);
        guardar = findViewById(R.id.guardar);


        return_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Ingresar_vacuna.this, Detalle_mascota.class);
                finish();
                startActivity(intent);
            }
        });

        logout_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Ingresar_vacuna.this, MainActivity.class);
                finish();
                startActivity(intent);
            }
        });

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Ingresar_vacuna.this, EstadoGuardado.class);
                startActivity(intent);
            }
        });


    }
}